//
//  ViewController.swift
//  Hello Single View
//
//  Created by Marcus Jakobsson on 2017-10-11.
//  Copyright © 2017 Marcus Jakobsson. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBAction func button_Click(_ sender: Any) {
        print("Hello button")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

